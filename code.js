
//let str = '||||*|||' //this is the original string in Broadbent
let str = '|||*||*||' //this is just to show  how it works with multiple multiplications

String.prototype.subsCapture = function (regex, sub){
	const match  =  this.match(regex)
	if(match){
		if (sub instanceof RegExp){ 
			const subMatch  = this.match(sub)
			if(subMatch){
				sub = subMatch[1]
			}
			else{
				return this
			}	
		}
		const index = match.index + match[0].indexOf(match[1])
		return this.slice(0,index) + sub + this.slice(index + match[1].length)
	}
	return this	//no matches
}

//rules:
//implemented as objects with properties:
//pattern: a pattern to find as a regex,the substitution is made to the capturing group
//subs: the string to substitute for, either a string or a regex, with the capturing 
//group as the string to use in the substitution
const rules =[
	//1?

	//2
	{pattern: /^\s*\+*(\|).*\*/, subs: '+'},
	
	//3
	{pattern: /^\s*\+*\*\+*(\|)/, subs: /^\s*(\+*)\*/},


	//these are for cleaning the output. They are slightly changed
	//to use spaces. 

	//4 
	//This is not a direct implmentation. Since Markov algorithms
	//need to apply the rule they find first, if it arrives here it means
	//that there are no '|' to the left or right of *, so all are 
	//either * or +
	{pattern: /^\s*(\+)\+*\*/, subs: ' '},
	//this is also rule 4
	{pattern: /^\s*(\*)/, subs: ' '},

	//5
	{pattern: /^\s*\|*(\+)/, subs: '|'},
]
console.log(`    ${str}`)
while(rules.find(({pattern,subs},index)=>{
		const nS = str.subsCapture(pattern,subs)
		const found =  nS != str //so it compares Str and string
		str = nS
		if(found){
			console.log(`${index}   ${str}`)
		}
		return found
	})){}


