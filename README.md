# Markov Algorithm from "The Deep Structure of Architecture," by Geoffrey Broadbent.
This is the Markov Algorithm described in the appendix of Geoffrey Broadbent, "The Deep Structure of Architecture" (reproduced verbatim below). The implementation
uses regular expressions to translate the algorithm described by  Broadbent. The full javascript code with comments is [here](code.js). 
From Geoffrey Broadbent, "The Deep Structure of Architecture", in Signs Symbols and Architecture (ed. Broadbent, Bunt, Jencks) p119-168

# Appendix
# Markov's Algorithm for Multiplying Two Numbers

Markov first sets up an alphabet, consisting of the letters, |, * and + such that his initial problem (multiply 4 X 3) is written |||| * |||. He then writes a set of rules for operating his algorithm: each step consists of rewriting the given sentence according to the first rule in Markov's list which can actually be applied to it.

1. If an asterisk occurs in a word more than once, it is reproduced without change.

2. If an asterisk occurs in a word exactly once and is preceded by at least one occurrence of a line then instead of the first occurrence of the line one substitutes a +.

3. If an asterisk occurs in a word exactly once and is not preceded by any lines, but lines occur following the asterisk, then the line occurring first is replaced by the left delimiter of the asterisk.

4. If an asterisk occurs in a word exactly once and no lines occur, the first letter of the word is struck out.

5. If the asterisk does not occur in a word but the plus sign does, then the first occurrence of the plus sign is replaced
by a line.

6. |f neither asterisk nor plus sign occurs in a word, then a step is impossible.

Only one of these rules may be applied at a time, so starting with the given word one applies the appropiate rule, and performs successive steps until one reaches a word to which none of the rules apply. Then one stops.

The code in this repository has been developed as part of *Sense Without Meaning*, a research project on code and architecture sponsored by [Vetenskapsrådet](https://www.vr.se/english.html), the Swedish Research Council. You can see the code running [here](https://zenbagailu.gitlab.io/markov-algorithm-from-broadbent-the-deep-structures-of-architecture/).
